namespace MonthReports.ConsoleNet6.Models;

public class BaseEntity
{
    public long Id { get; set; }
}