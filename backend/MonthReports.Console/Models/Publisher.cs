using System;

namespace MonthReports.ConsoleNet6.Models 
{

    public class Publisher : BaseEntity
    {        

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }

        public int Age 
        {
            get { return DateTime.Now.Year - Birthdate.Year ;}
        }
        

        public DateTime Birthdate { get; set; }

        public DateTime Baptismdate { get; set; }

        public bool Anointed { get; set; }
        public bool Elder { get; set; }
        public bool MinisterialServant { get; set; }
        public bool RegularPioneer { get; set; }
        public Genders Gender { get; set; }
    }
}