class Vehicle
{
    public int PassengersCapacity { get; set; }

    public void Run() {

    }
}

class FueledVehicle : Vehicle
{
    public string FuelType { get; set; }
    public int FuleCapacity { get; set; }
}

class UnfueledVehicle : Vehicle
{

}

class Bicycle : UnfueledVehicle 
{

}

class Pruebas 
{
    void Probar() 
    {
        var myVehicle = new Vehicle();
        var myFueledVehicle = new FueledVehicle();
        myFueledVehicle.FuleCapacity = 30;
        myFueledVehicle.FuelType = "Nafta";


        var myUnfueledVehicle = new UnfueledVehicle();
        
        
        var myBicycle  = new Bicycle();
        myBicycle.PassengersCapacity = 1;
        myBicycle.Run();
    }
}