﻿using System;
using MonthReports.ConsoleNet6.Models;
using MonthReports.ConsoleNet6.Services;

namespace MonthReports.ConsoleNet6
{
    class Program
    {
        static void Main(string[] args)
        {    
            var publisher1 = new Publisher();
            publisher1.FirstName = "Juan";
            publisher1.LastName = "Perez";
            publisher1.Birthdate = new DateTime(1980,01,01);
            publisher1.Baptismdate = new DateTime(2000,01,01);
            publisher1.Elder = false;
            publisher1.Gender = Genders.Male;

            var mayMonthReport = new MonthReport();
            mayMonthReport.Hours = 20;
            mayMonthReport.Placements = 10;
            mayMonthReport.Publisher = publisher1;

            Console.WriteLine("Hola " + publisher1.FullName);
            Console.WriteLine("{0} tiene {1} años de edad", publisher1.FullName, publisher1.Age);

            var publisher2 = new Publisher();

            var publisherRepository = new GenericRepository<Publisher>();
            var monthReportRepository = new GenericRepository<MonthReport>();
            
            
            monthReportRepository.Add(mayMonthReport);
            publisherRepository.Add(publisher1);
            publisherRepository.Add(publisher2);
            

            Console.WriteLine("Tenemos {0} publicadores", publisherRepository.GetAll().Count);

            publisherRepository.Remove(1);

            Console.WriteLine("Tenemos {0} publicadores", publisherRepository.GetAll().Count);

            publisherRepository.Remove(5);
        }
    }
}

