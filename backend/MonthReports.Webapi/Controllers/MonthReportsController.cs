using Microsoft.AspNetCore.Mvc;
using System.Linq;
using MonthReports.Webapi.Models;

namespace MonthReports.Webapi.Controllers;

[ApiController]
// https://loquesea.com/api/monthreports
[Route("api/[controller]")]
public class MonthReportsController
{
    private MonthReportsRepository _repository;

    public MonthReportsController(MonthReportsRepository repository)
    {
        _repository = repository;
    }


    //GET https://loquesea.com/api/monthreports
    [HttpGet]
    public List<MonthReport> Get() 
    {  
        var query = _repository.GetQuery();

        var rv = query.ToList();

        return rv;
    }

    //POST https://loquesea.com/api/monthreports
    [HttpPost]
    public void Post(MonthReport dto) 
    {
        _repository.Add(dto);
    }

    //DELETE https://loquesea.com/api/monthreports
    [HttpDelete("{id}")]
    public void Delete([FromRoute]long id) {
        _repository.Remove(id);
    }

    //PUT https://loquesea.com/api/monthreports/3
    [HttpPut("{id}")]
    public void Update([FromRoute]long id, [FromBody]MonthReport dto)
    {
        _repository.Update(id, dto);
    }

}