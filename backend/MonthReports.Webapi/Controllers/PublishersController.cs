using Microsoft.AspNetCore.Mvc;
using System.Linq;
using MonthReports.Webapi.Models;

namespace MonthReports.Webapi.Controllers;

[ApiController]
// https://loquesea.com/api/publishers
[Route("api/[controller]")]
public class PublishersController
{
    private PublishersRepository _publisherRepository;
    private readonly ILogger llll;

    public PublishersController(PublishersRepository publishersRepository,
        ILogger<PublishersController> logger)
    {
        _publisherRepository = publishersRepository;
        llll  = logger;
    }


    //GET https://loquesea.com/api/publishers
    [HttpGet]
    public List<Publisher> GetPublishers([FromQuery]bool? isElder = null, [FromQuery]string? lastName = null) 
    {  
        llll.LogInformation("Obtenindo datos de publicador");
        throw new Exception("Error de prueba");
        Console.WriteLine("Procesando el request");
        var query = _publisherRepository.GetQuery();

        if(isElder != null)
        {
            query = query.Where(p => p.Elder == isElder);
        }

        if(lastName != null)
        {
            query = query.Where(p => p.LastName.ToLower() == lastName.ToLower());
        }

        var rv = query.ToList();


        return rv;
    }

    //GET https://loquesea.com/api/publishers/count
    [HttpGet("count")]
    public int GetPublishersCount([FromQuery]bool? isElder = null, [FromQuery]string? lastName = null) 
    {      

        var query = _publisherRepository.GetQuery();

        if(isElder != null)
        {
            query = query.Where(p => p.Elder == isElder);
        }

        if(lastName != null)
        {
            query = query.Where(p => p.LastName.ToLower() == lastName.ToLower());
        }

        var rv = query.Count();

        return rv;
    }


    //POST https://loquesea.com/api/publishers
    [HttpPost]
    public void PostPublisher(Publisher dto) 
    {
        _publisherRepository.Add(dto);
    }

    //DELETE https://loquesea.com/api/publishers/3
    [HttpDelete("{id}")]
    public void DeletePublisher([FromRoute]long id) {
        _publisherRepository.Remove(id);
    }

    //PUT https://loquesea.com/api/publishers/3
    [HttpPut("{id}")]
    public void UpdatePublisher([FromRoute]long id, [FromBody]Publisher dto)
    {
        _publisherRepository.Update(id, dto);
    }

}