using Microsoft.AspNetCore.Mvc;
using MonthReports.Webapi.Services.Time;

namespace MonthReports.Webapi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TestController : ControllerBase
{
    private int _myVar = 0;
    private ITimeService _timeService;

    public TestController(ITimeService timeService) 
    {
        _timeService = timeService;
    }

    //GET http://urldeaplicacion/api/test
    [HttpGet]
    public string DameHolaMundo([FromQuery]string? name = null,
                                [FromQuery]string? color = null) 
    {
        var rv = @$"Hola mundo!
        Parametro 'name' (de queryParam): {name}
        Parametro 'color' (de queryParam): {color}";


        return rv;
    }

    //GET http://urldeaplicacion/api/test/chau
    [HttpGet("chau")]
    public string DameChauMundo() 
    {
        return "Chau mundo!";
    }

    [HttpGet("time")]
    public string GetDateTime()
    {
        
        return _timeService.GetDateTime();
    }

    [HttpPost("myvar")]
    public void PostMyVar(int value)
    {
        _myVar = value;
    }

    [HttpGet("myvar")]
    public int GetMyVar()
    {
        return _myVar;
    }


    [HttpPost("timezone")]
    public void PostTimezone(string timezone)
    {
        _timeService.Timezone = timezone;
    }

    [HttpGet("timezone")]
    public string GetTimezone()
    {
        return _timeService.Timezone;
    }

}