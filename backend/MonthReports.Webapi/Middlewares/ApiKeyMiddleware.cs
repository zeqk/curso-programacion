namespace MonthReports.Webapi.Middlewares;

public class ApiKeyMiddleware
{
    private readonly RequestDelegate _next;
    public ApiKeyMiddleware(RequestDelegate next) 
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        if(context.Request.Path.Value.Contains("api")) 
        {
            var apiKey = context.Request.Headers["X-Api-Key"];
            if(apiKey != "654321")
                throw new AuthException("La api key es incorrecta");

        }
        await _next(context);
    }
}