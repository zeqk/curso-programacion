public class ExceptionHandlingMiddleware 
{
    private readonly RequestDelegate _next;
    private readonly ILogger _logger;
    public ExceptionHandlingMiddleware(RequestDelegate next,
            ILogger<ExceptionHandlingMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task Invoke(HttpContext context)
    {
        var scopeData = new Dictionary<string,object>() { ["CorrelationId"] = context.TraceIdentifier };

        var scope =_logger.BeginScope(scopeData);
        _logger.LogDebug("Ejecutando " + context.Request.Path);
        scope.Dispose();


        using(_logger.BeginScope(scopeData))
        {
            try
            {            
                _logger.LogDebug("Ejecutando " +context.Request.Path);
                await _next(context);
            }
            catch (System.Exception ex)
            {
            
                var problem = new Microsoft.AspNetCore.Mvc.ProblemDetails()
                {
                    Title = "Error inesperado",
                    Detail = ex.Message
                };
                problem.Extensions["CorrelationId"] = context.TraceIdentifier;

                context.Response.StatusCode = 500;
                context.Response.ContentType = "application/problem+json";

                if(ex is AuthException)
                {
                    context.Response.StatusCode = 401;
                    problem.Title = "Error de autenticacion";
                    
                    _logger.LogError(ex, "Error de autenticacion");
                }
                else
                {
                    _logger.LogError(ex, "Error inesperado");
                }


                var result = Newtonsoft.Json.JsonConvert.SerializeObject(problem, new Newtonsoft.Json.JsonSerializerSettings
                {
                    ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
                });
                await context.Response.WriteAsync(result);

            }
            
        }
        
    }
}