namespace MonthReports.Webapi.Models;
public class MonthReport : BaseEntity
{
    public int Month { get; set; }
    public int ServiceYear { get; set; }
    public int Hours { get; set; }
    public int Placements { get; set; }
    public int Videos { get; set; }
    public int ReturnVisits { get; set; }
    public int Studies { get; set; }
    public string Remarks { get; set; }

    public Publisher Publisher { get; set; }
}