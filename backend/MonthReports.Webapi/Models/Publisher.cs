namespace MonthReports.Webapi.Models;

public class Publisher : BaseEntity
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public DateTime BirthDate { get; set; }
    public DateTime? BaptismDate { get; set; }
    public bool Anointed { get; set; }
    public bool Elder { get; set; }
    public bool MinisterialServant { get; set; }
    public bool RegularPioneer { get; set; }
}