using MonthReports.Webapi.Services.Time;
using MonthReports.Webapi.Middlewares;
using NSwag.Generation.Processors.Security;

namespace MonthReports.Webapi;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.

        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        

        // Register the Swagger services
        // builder.Services.AddSwaggerDocument(config => {
        //     config.Title = "MonthReports";
        // });

        builder.Services.AddOpenApiDocument(config => {
            config.Title = "MonthReports";

            config.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("apiKeyAuth"));

            config.AddSecurity("apiKeyAuth", Enumerable.Empty<string>(), new NSwag.OpenApiSecurityScheme {
                Type = NSwag.OpenApiSecuritySchemeType.ApiKey,
                In = NSwag.OpenApiSecurityApiKeyLocation.Header,
                Name = "X-Api-Key"
            });

        });

        builder.Services.AddSingleton<PublishersRepository>();
        builder.Services.AddSingleton<MonthReportsRepository>();
        builder.Services.AddSingleton<ITimeService,FrenchTimeService>();

        // builder.Services.AddScoped(typeof(PublishersRepository));

        var app = builder.Build();

        app.UseMiddleware<ExceptionHandlingMiddleware>();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseOpenApi();
            app.UseSwaggerUi3();
        }

        app.UseCors(config => 
        {
            config.AllowAnyHeader();
            config.AllowAnyMethod();
            config.AllowAnyOrigin();
        });

        app.UseAuthorization();


        app.MapControllers();

        app.UseMiddleware<ApiKeyMiddleware>();

        // app.Run(async context => {
        //     await context.Response.WriteAsync("Holaaa soy el ultimo middleware");
        // });

        app.Run();

        //Middleware 2
        // app.Use(async (context, next) => {

        //     await next();

        // });

        // app.Run();


        // ///a MODO PRUEBA
        // var publishersRepository = new PublishersRepository();
        // var controller = new MonthReports.Webapi.Controllers.PublishersController(publishersRepository);


        // controller.GetPublishers();
        // controller.GetPublishersCount();



    }
}
