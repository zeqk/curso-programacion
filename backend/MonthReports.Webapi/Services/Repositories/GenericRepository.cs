using MonthReports.Webapi.Models;

namespace MonthReports.Webapi.Services.Repositories
{
    public class GenericRepository<T> where T : BaseEntity
    {
        protected List<T> _internalList;

        public GenericRepository()
        {

            _internalList = new List<T>();
        }

        public IQueryable<T> GetQuery() 
        {
            return _internalList.AsQueryable();
        }

        public void Add(T model)
        {
            if(_internalList.Count > 0) 
            {
                model.Id = _internalList.Max(m => m.Id) + 1;
            }
            else {
                model.Id = 1;
            }
            _internalList.Add(model);
        }

        public T Get(long id)
        {
            return _internalList.Where(m => m.Id == id).FirstOrDefault();
        }

        public List<T> GetAll() {
            return _internalList;
        }

        public void Remove(long id)
        {
            var publisherToRemove = Get(id);

            var wasRemoved = _internalList.Remove(publisherToRemove);
            // if(!wasRemoved)
            //     throw new Exception("No existe el publicador");
        }

        public void Update(long id, T model) 
        {
            Remove(id);
            model.Id = id;
            _internalList.Add(model);
        }
    }
}