using MonthReports.Webapi.Models;
using MonthReports.Webapi.Services.Repositories;
public class PublishersRepository : GenericRepository<Publisher>
{
    public PublishersRepository()
    {
        _internalList = new List<Publisher> {
            new Publisher{
                Id = 1,
                FirstName = "Mateo",
                LastName = "Carlos",
                BirthDate = new DateTime(1980,01,01),
                Elder = true
            },
            new Publisher{
                Id = 2,
                FirstName = "Marcos",
                LastName = "Carlos",
                BirthDate = new DateTime(1980,01,01),
                Elder = false
            },
            new Publisher{
                Id = 3,
                FirstName = "Clara",
                LastName = "Perez",
                BirthDate = new DateTime(1980,01,01),
                Elder = false
            },
            new Publisher{
                Id = 4,
                FirstName = "Carolina",
                LastName = "Gonzalez",
                BirthDate = new DateTime(1980,01,01),
                Elder = false,
                RegularPioneer = true
            }
        };
    }
}