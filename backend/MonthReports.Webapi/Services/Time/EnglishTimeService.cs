using System.Globalization;

namespace MonthReports.Webapi.Services.Time;

public class EnglishTimeService : ITimeService
{

    public string Timezone { get; set; }

    public string GetDateTime()
    {
        return DateTime.Now.ToString("D", CultureInfo.GetCultureInfo("en-US"));
    }
}