using System.Globalization;
public class FrenchTimeService : ITimeService
{
    public string Timezone { get; set; }

    public string GetDateTime()
    {
        return DateTime.Now.ToString("D", CultureInfo.GetCultureInfo("fr-FR"));
    }
}