public interface ITimeService 
{
    string Timezone { get; set; }
    string GetDateTime();
}