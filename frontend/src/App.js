import { Routes, Route } from "react-router-dom";

import { Navigation } from "./common";
import { Dashboard, Publishers, PublisherForm, Reports } from "./pages";

export default function App() {
  return (
    <div className="container">
      <Navigation title="Reports" theme="dark" />

      <section className="py-2">
        <Routes>
          <Route index path="/" element={<Dashboard />} />
          <Route path="publishers" element={<Publishers />} />
          <Route path="publishers/new" element={<PublisherForm />} />
          <Route path="reports" element={<Reports />} />
        </Routes>
      </section>
    </div>
  );
}
