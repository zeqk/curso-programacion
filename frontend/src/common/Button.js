export default function Button({
  label,
  type = "button",
  variant = "primary",
  size = "sm",
  iconName,
  onClick,
  loading,
}) {
  return (
    <button
      type={type}
      className={`btn btn-${variant} btn-${size}`}
      onClick={onClick}
      disabled={loading}
    >
      {iconName && <i className={`bi bi-${iconName}`} />}
      {!loading && label}
      {loading && (
        <>
          <span
            className="spinner-border spinner-border-sm"
            role="status"
            aria-hidden="true"
          />
          {label}
        </>
      )}
    </button>
  );
}
