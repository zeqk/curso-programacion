import { useLocation } from "react-router-dom";

import NavigationLink from "./NavigationLink";

export default function Navigation({ title, theme = "light" }) {
  const { pathname } = useLocation();
  return (
    <nav className={`navbar navbar-expand-sm navbar-${theme} bg-${theme}`}>
      <div className="container-fluid">
        <a className="navbar-brand" href="/">
          {title}
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <NavigationLink
              title="Dashboard"
              to="/"
              active={pathname === "/"}
            />
            <NavigationLink
              title="Publishers"
              to="publishers"
              active={pathname === "/publishers"}
            />
            <NavigationLink
              title="Reports"
              to="/reports"
              active={pathname === "/report"}
            />
          </ul>
        </div>
      </div>
    </nav>
  );
}
