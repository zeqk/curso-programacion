export default function Privilege({ variant, label }) {
  return <span class={`badge bg-${variant} mx-1`}>{label}</span>;
}
