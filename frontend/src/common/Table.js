import { Loading } from "../common";

export default function Table({ columns, data, loading, onRowClick }) {
  return (
    <table className="table">
      <thead>
        <tr>
          {columns.map((col) => (
            <th key={col.key} scope="col">
              {col.label}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map((row) => (
          <tr key={row.id} onClick={() => onRowClick(row)}>
            {columns.map((col) => (
              <td key={col.key} style={{ cursor: "pointer" }}>
                {col.render ? col.render(row) : row[col.key]}
              </td>
            ))}
          </tr>
        ))}
        {loading && (
          <tr>
            <td className="text-center" colSpan={columns.length}>
              <Loading />
            </td>
          </tr>
        )}
        {data.length === 0 && !loading && (
          <tr>
            <td className="text-center" colSpan={columns.length}>
              No data.
            </td>
          </tr>
        )}
      </tbody>
    </table>
  );
}
