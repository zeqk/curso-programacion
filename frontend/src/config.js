const config = {
  apiUrl: process.env.REACT_APP_API_URL,
  mockApi: process.env.REACT_APP_USE_MOCK === "true",
};

export default config;
