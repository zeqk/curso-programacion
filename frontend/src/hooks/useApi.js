import { useState } from "react";

import config from "../config";

export default function useApi() {
  const [loading, setLoading] = useState(false);

  const wait = (ms = 2500) => {
    setLoading(true);
    return new Promise((resolve) =>
      setTimeout(() => {
        setLoading(false);
        resolve();
      }, ms)
    );
  };

  const mock = {
    loading,
    get: async () => {
      await wait();
      return [
        {
          id: 1,
          firstName: "Daniel",
          lastName: "Ballesteros",
          ministerialServant: true,
          baptismDate: null,
          birthDate: "1989-06-01T13:45:30",
        },
        {
          id: 2,
          firstName: "Silvina",
          lastName: "Moyano",
          elder: false,
          regularPioneer: true,
          anointed: true,
          baptismDate: "2009-06-01T13:45:30",
          birthDate: "1989-06-01T13:45:30",
        },
        {
          id: 3,
          firstName: "Jens",
          lastName: "Tonk",
          elder: true,
          regularPioneer: true,
          anointed: false,
          baptismDate: "2009-06-01T13:45:30",
          birthDate: "1989-06-01T13:45:30",
        },
      ];
    },
    post: async () => {
      await wait();
      return;
    },
    patch: async () => {
      await wait();
      return;
    },
    remove: async () => {
      await wait();
      return;
    },
  };

  async function get(url) {
    setLoading(true);
    const res = await fetch(`${config.apiUrl}/${url}`);
    setLoading(false);
    if (res.ok) {
      return res.json();
    } else {
      throw Error(res);
    }
  }

  async function post(url, data) {
    setLoading(true);
    const res = await fetch(`${config.apiUrl}/${url}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    setLoading(false);
    if (res.ok) {
      return res.json();
    } else {
      throw Error(res);
    }
  }
  async function patch(url, id, data) {
    setLoading(true);
    const res = await fetch(`${config.apiUrl}/${url}/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    setLoading(false);
    if (res.ok) {
      return res.json();
    } else {
      throw Error(res);
    }
  }

  async function remove(url, id) {
    setLoading(true);
    const res = await fetch(`${config.apiUrl}/${url}/${id}`, {
      method: "DELETE",
    });
    setLoading(false);
    if (res.ok) {
      return res.json();
    } else {
      throw Error(res);
    }
  }

  if (config.mockApi) {
    return mock;
  }

  return {
    get,
    post,
    patch,
    remove,
    loading,
  };
}
