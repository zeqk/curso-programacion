import { useState } from "react";

export default function useForm({ initialValues, validate }) {
  const [values, setValues] = useState(initialValues || {});
  const [errors, setErrors] = useState({});

  function handleChange(key, value) {
    console.log(key, value);
    setValues({ ...values, [key]: value });
    setErrors({});
  }

  function handleSubmit(cb) {
    const err = validate(values);
    setErrors(err);
    if (Object.keys(err).length === 0) {
      cb();
    }
  }

  return {
    values,
    errors,
    handleChange,
    handleSubmit,
  };
}
