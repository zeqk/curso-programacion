import dayjs from "dayjs";
import { useLocation, useNavigate } from "react-router-dom";

import { Button, Checkbox, Input } from "../common";
import { useApi, useForm } from "../hooks";

function validate(values) {
  const err = {};
  if (!values.firstName) err.firstName = "Name is required";
  if (!values.lastName) err.lastName = "Surname is required";
  if (!values.birthDate) err.birthDate = "Birth date is required";
  console.log(values);
  if (values.elder && values.ministerialServant)
    err.elder = "Choose one option";
  return err;
}

export default function PublisherForm() {
  const api = useApi();
  const navigate = useNavigate();
  const { state } = useLocation();
  const { values, errors, handleChange, handleSubmit } = useForm({
    initialValues: state,
    validate,
  });

  const onSubmit = (e) => {
    e.preventDefault();

    handleSubmit(async () => {
      if (values.id > 0) {
        await api.patch("publishers", values.id, values);
      } else {
        await api.post("publishers", values);
      }
      navigate("/publishers");
    });
  };

  return (
    <form onSubmit={onSubmit}>
      <h3>
        {values.id > 0
          ? `${values.firstName} ${values.lastName}`
          : "New publisher"}
      </h3>
      <div className="mb-3">
        <Input
          name="firstName"
          label="Name"
          value={values.firstName}
          error={errors.firstName}
          handleChange={handleChange}
        />
      </div>
      <div className="mb-3">
        <Input
          name="lastName"
          label="Surname"
          value={values.lastName}
          error={errors.lastName}
          handleChange={handleChange}
        />
      </div>
      <div className="mb-3">
        <Input
          type="date"
          name="birthDate"
          label="Birth date"
          value={dayjs(values.birthDate).format("YYYY-MM-DD")}
          error={errors.birthDate}
          handleChange={handleChange}
        />
      </div>
      <div className="mb-3">
        <Input
          type="date"
          name="baptismDate"
          label="Baptism date"
          value={dayjs(values.baptismDate).format("YYYY-MM-DD")}
          error={errors.baptismDate}
          handleChange={handleChange}
        />
      </div>
      <div className="mb-3">
        <Checkbox
          label="Elder"
          name="elder"
          handleChange={handleChange}
          error={errors.elder}
          value={values.elder}
        />
        <Checkbox
          label="Ministerial servant"
          name="ministerialServant"
          handleChange={handleChange}
          error={errors.elder}
          value={values.ministerialServant}
        />
        <Checkbox
          label="Regular pioneer"
          name="regularPioneer"
          handleChange={handleChange}
          value={values.regularPioneer}
        />
        <Checkbox
          label="Annointed"
          name="anointed"
          handleChange={handleChange}
          value={values.anointed}
        />
      </div>

      <Button type="submit" label="Submit" size="md" loading={api.loading} />
    </form>
  );
}
