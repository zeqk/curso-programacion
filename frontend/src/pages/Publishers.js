import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import { Button, Table } from "../common";
import { useApi } from "../hooks";
import { renderDate, renderPrivileges } from "../util/renderFunctions";

export default function Publishers() {
  const api = useApi();
  const navigate = useNavigate();
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch();
  }, []);

  async function fetch() {
    const res = await api.get("publishers");
    setData(res);
  }

  async function handleDelete(event, { id }) {
    event.stopPropagation();
    await api.remove("publishers", id);
    fetch();
  }

  function handleRowClick(data) {
    navigate("/publishers/new", { state: data });
  }

  function handleNew() {
    navigate("/publishers/new");
  }

  const columns = [
    { key: "id", label: "#" },
    { key: "firstName", label: "Name" },
    { key: "lastName", label: "Surname" },
    {
      key: "baptismDate",
      label: "Baptism date",
      render: (row) => renderDate(row.baptismDate),
    },
    {
      key: "birthDate",
      label: "Birth date",
      render: (row) => renderDate(row.birthDate),
    },
    {
      key: "privileges",
      label: "Privileges",
      render: renderPrivileges,
    },
    {
      key: "actions",
      label: "",
      render: (row) => (
        <Button
          variant="danger"
          iconName="trash"
          onClick={(event) => handleDelete(event, row)}
        />
      ),
    },
  ];

  return (
    <>
      <Button label="New publisher" onClick={handleNew} />
      <Table
        columns={columns}
        data={data}
        loading={api.loading}
        onRowClick={handleRowClick}
      />
    </>
  );
}
