import dayjs from "dayjs";
import { Privilege } from "../common";

function renderDate(date) {
  if (!date) {
    return "";
  }
  return dayjs(date).format("D/M/YYYY");
}

function renderPrivileges(row) {
  let res = [];
  if (row.elder) {
    res.push(<Privilege label="E" variant="primary" />);
  }
  if (row.ministerialServant) {
    res.push(<Privilege label="MS" variant="success" />);
  }
  if (row.anointed) {
    res.push(<Privilege label="A" variant="info" />);
  }
  if (row.regularPioneer) {
    res.push(<Privilege label="RP" variant="danger" />);
  }
  return res;
}

export { renderDate, renderPrivileges };
